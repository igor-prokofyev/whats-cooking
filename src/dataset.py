import json
import re
import zipfile
from pathlib import Path
from typing import Tuple

import nltk
import pandas as pd
import requests
import typer
from loguru import logger
from nltk.stem import WordNetLemmatizer
from sklearn.model_selection import train_test_split

from src.config import EXTERNAL_DATA_DIR, PROCESSED_DATA_DIR, RAW_DATA_DIR

app = typer.Typer()

# Ensure raw data is available
urls = [
    "http://92.63.176.253:8080/downloads/ci_dataset.json",
    "http://92.63.176.253:8080/downloads/dataset.json",
]

# Create the directory if it doesn't exist
RAW_DATA_DIR.mkdir(parents=True, exist_ok=True)

for url in urls:
    logger.info("Downloading dataset {}", url)
    response = requests.get(url)
    filename = url.split("/")[-1]
    filepath = RAW_DATA_DIR / filename
    with open(filepath, "wb") as file:
        file.write(response.content)
    logger.info("Downloaded {}", filename)

# Ensure NLTK WordNet is available
try:
    nltk.data.find("wordnet.zip")
except LookupError:
    nltk.download("wordnet", download_dir=EXTERNAL_DATA_DIR)
    with zipfile.ZipFile(EXTERNAL_DATA_DIR / "corpora" / "wordnet.zip", "r") as z:
        z.extractall(EXTERNAL_DATA_DIR / "corpora")
    nltk.data.path.append(EXTERNAL_DATA_DIR)


def load_data(file_path: Path) -> pd.DataFrame:
    """
    Load data from a JSON file and return a pandas DataFrame.

    Args:
        file_path (Path): Path to the JSON file.

    Returns:
        pd.DataFrame: Loaded data as a DataFrame.
    """
    with open(file_path, "r") as file:
        data = json.load(file)
    return pd.DataFrame(data)


def preprocess_df(df: pd.DataFrame) -> pd.DataFrame:
    """
    Preprocess the DataFrame by cleaning and lemmatizing the 'ingredients' column.

    Args:
        df (pd.DataFrame): Input DataFrame.

    Returns:
        pd.DataFrame: Preprocessed DataFrame.
    """

    def process_string(x: str) -> str:
        x = [
            " ".join([WordNetLemmatizer().lemmatize(q) for q in p.split()]) for p in x
        ]  # Lemmatization
        x = list(
            map(
                lambda x: re.sub(
                    r"\(.*oz.\)|crushed|crumbles|ground|minced|powder|chopped|sliced", "", x
                ),
                x,
            )
        )  # Remove unnecessary words
        x = list(map(lambda x: re.sub("[^a-zA-Z]", " ", x), x))  # Remove all non-letter characters
        x = " ".join(x)  # Convert list to string
        x = x.lower()  # Convert to lowercase
        return x

    df = df.drop("id", axis=1)  # Remove 'id' column
    df["ingredients"] = df["ingredients"].apply(
        process_string
    )  # Apply cleaning function to 'ingredients' column

    return df


def split_data(
    df: pd.DataFrame, test_size: float = 0.2, random_state: int = 42
) -> Tuple[pd.DataFrame, pd.DataFrame]:
    """
    Split the DataFrame into training and testing sets.

    Args:
        df (pd.DataFrame): Input DataFrame.
        test_size (float): Proportion of the dataset to include in the test split.
        random_state (int): Random seed for reproducibility.

    Returns:
        Tuple[pd.DataFrame, pd.DataFrame]: Training and testing DataFrames.
    """
    if df["cuisine"].value_counts().min() < 2:
        train_df, test_df = train_test_split(df, test_size=test_size, random_state=random_state)
    else:
        train_df, test_df = train_test_split(
            df, test_size=test_size, random_state=random_state, stratify=df["cuisine"]
        )
    return train_df, test_df


def save_data(df: pd.DataFrame, file_path: Path) -> None:
    """
    Save the DataFrame to a JSON file.

    Args:
        df (pd.DataFrame): DataFrame to save.
        file_path (Path): Path to the JSON file.
    """
    file_path = Path(file_path)
    file_path.parent.mkdir(parents=True, exist_ok=True)
    df.to_json(file_path, orient="records", lines=True)


@app.command()
def main(
    input_file: Path = RAW_DATA_DIR / "dataset.json",
    train_file: Path = PROCESSED_DATA_DIR / "train.json",
    test_file: Path = PROCESSED_DATA_DIR / "test.json",
    test_size: float = 0.2,
    random_state: int = 42,
) -> None:
    """
    Main function to process the dataset.

    Args:
        input_file (Path): Path to the input JSON file.
        train_file (Path): Path to save the training data.
        test_file (Path): Path to save the testing data.
        test_size (float): Proportion of the dataset to include in the test split.
        random_state (int): Random seed for reproducibility.
    """
    logger.info("Processing dataset...")

    df = load_data(input_file)

    # Clean the data
    df = preprocess_df(df)

    train_df, test_df = split_data(df, test_size=test_size, random_state=random_state)
    save_data(train_df, train_file)
    save_data(test_df, test_file)

    logger.success("Data has been cleaned, split and saved to {} and {}", train_file, test_file)


if __name__ == "__main__":
    app()
