import os
from pathlib import Path
import mlflow
import pandas as pd
import typer
from dotenv import load_dotenv
from loguru import logger
from mlflow.models.signature import infer_signature
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import GridSearchCV
from sklearn.svm import SVC
import joblib
import tempfile

from src.config import PROCESSED_DATA_DIR

app = typer.Typer()

load_dotenv()
remote_server_uri = os.getenv("MLFLOW_TRACKING_URI")
mlflow.set_tracking_uri(remote_server_uri)


def load_data(file_path: Path) -> pd.DataFrame:
    """
    Load data from a JSON lines file.

    Args:
        file_path (Path): Path to the JSON lines file.

    Returns:
        pd.DataFrame: Loaded data as a DataFrame.
    """
    return pd.read_json(file_path, lines=True)


def train_model(train_file: Path, test_file: Path, n_splits: int = 5) -> None:
    """
    Train an SVM model on the training data and evaluate on the test data.

    Args:
        train_file (Path): Path to the processed training data file.
        test_file (Path): Path to the processed testing data file.
        n_splits (int): Number of splits for cross-validation.
    """

    # Start MLflow experiment
    experiment_name = "whats_cooking"
    mlflow.set_experiment(experiment_name)
    with mlflow.start_run():
        # Load the processed training and testing data
        train_df = load_data(train_file)
        test_df = load_data(test_file)

        # Separate features and target
        X_train = train_df["ingredients"]
        y_train = train_df["cuisine"]
        X_test = test_df["ingredients"]

        # TF-IDF Vectorizer
        tfidf = TfidfVectorizer(
            stop_words="english",
            ngram_range=(1, 1),
            max_df=0.57,
            token_pattern=r"\w+",
            sublinear_tf=False,
        )
        X_train_tfidf = tfidf.fit_transform(X_train)
        X_test_tfidf = tfidf.transform(X_test)  # noqa F841

        # Define the model and parameter grid for GridSearch
        param_grid = {
            "C": [0.001, 0.1, 1, 10, 50, 100, 500, 1000, 5000],
            "gamma": [0.0001, 0.001, 0.01, 0.1, 1, 10, 100],
            "kernel": ["rbf"],
        }

        grid = GridSearchCV(
            SVC(), param_grid, refit=True, verbose=3, n_jobs=-1, scoring="accuracy", cv=n_splits
        )

        # Train the model
        grid.fit(X_train_tfidf, y_train)

        # Log best parameters and score
        logger.info("Best parameters found: {}", grid.best_params_)
        logger.info("Best accuracy score: {}", grid.best_score_)

        mlflow.log_params(grid.best_params_)
        mlflow.log_metric("best_accuracy", grid.best_score_)

        # Define signature
        signature = infer_signature(X_train_tfidf, y_train)

        # Log the model
        mlflow.sklearn.log_model(
            sk_model=grid.best_estimator_,
            artifact_path="model",
            registered_model_name="cuisine_svc_model",
            signature=signature,
        )

        with tempfile.TemporaryDirectory() as temp_dir:
            tfidf_path = os.path.join(temp_dir, "tfidf_vectorizer.pkl")
            joblib.dump(tfidf, tfidf_path)
            mlflow.log_artifact(tfidf_path)

    # Retrieve the best run ID
    experiment = mlflow.get_experiment_by_name(experiment_name)
    experiment_id = experiment.experiment_id
    df = mlflow.search_runs([experiment_id])
    best_run_id = df.loc[0, "run_id"]
    logger.info("Best run_id:  {}", best_run_id)


@app.command()
def main(
    train_file: Path = PROCESSED_DATA_DIR / "train.json",
    test_file: Path = PROCESSED_DATA_DIR / "test.json",
    n_splits: int = 5,
) -> None:
    """
    Main function to train the model using default file paths or provided paths.

    Args:
        train_file (Path): Path to the training data file.
        test_file (Path): Path to the testing data file.
        n_splits (int): Number of splits for cross-validation.
    """
    logger.info("Training model...")
    train_model(train_file, test_file, n_splits)
    logger.success("Model training complete")


if __name__ == "__main__":
    app()
