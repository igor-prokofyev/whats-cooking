import json
import sys
import tempfile
from pathlib import Path
from typing import Generator

import pandas as pd
import pytest
from typer.testing import CliRunner

sys.path.append(str(Path(__file__).parent.parent))

from src.dataset import app, load_data, save_data, split_data  # noqa E402

runner = CliRunner()

# Mock data for testing
mock_data = [
    {"id": 1, "cuisine": "italian", "ingredients": ["tomato", "basil"]},
    {"id": 2, "cuisine": "mexican", "ingredients": ["avocado", "lime"]},
    {"id": 3, "cuisine": "italian", "ingredients": ["garlic", "pasta"]},
    {"id": 4, "cuisine": "japanese", "ingredients": ["rice", "fish"]},
]


@pytest.fixture
def mock_json_file() -> str:
    """
    Create a temporary JSON file containing mock data for testing.
    """
    with tempfile.NamedTemporaryFile(delete=False, mode="w") as f:
        json.dump(mock_data, f)
        return f.name


@pytest.fixture
def temp_dir() -> Generator[Path, None, None]:
    """
    Create a temporary directory for testing file output.
    """
    with tempfile.TemporaryDirectory() as tmpdirname:
        yield Path(tmpdirname)


def test_load_data(mock_json_file: str) -> None:
    """
    Test the load_data function to ensure it loads data correctly.
    """
    df = load_data(mock_json_file)
    assert not df.empty
    assert len(df) == 4
    assert set(df.columns) == {"id", "cuisine", "ingredients"}


def test_split_data() -> None:
    """
    Test the split_data function to ensure it splits data into train and test sets correctly.
    """
    df = pd.DataFrame(mock_data)
    train_df, test_df = split_data(df, test_size=0.25, random_state=42)
    assert len(train_df) == 3
    assert len(test_df) == 1
    assert set(train_df["cuisine"]).issubset(set(df["cuisine"]))
    assert set(test_df["cuisine"]).issubset(set(df["cuisine"]))


def test_save_data(temp_dir: Path) -> None:
    """
    Test the save_data function to ensure it saves data to a file correctly.
    """
    df = pd.DataFrame(mock_data)
    save_path = temp_dir / "output.json"
    save_data(df, save_path)
    assert save_path.exists()
    loaded_data = pd.read_json(save_path, lines=True)
    assert len(loaded_data) == 4
    assert loaded_data.iloc[0]["cuisine"] == "italian"


def test_main(mock_json_file: str, temp_dir: Path) -> None:
    """
    Test the main CLI command to ensure it runs without errors and produces the expected output files.
    """
    train_file = temp_dir / "train.json"
    test_file = temp_dir / "test.json"

    result = runner.invoke(
        app,
        [
            "--input-file",
            mock_json_file,
            "--train-file",
            train_file,
            "--test-file",
            test_file,
            "--test-size",
            "0.25",
            "--random-state",
            "42",
        ],
    )

    assert result.exit_code == 0
    assert train_file.exists()
    assert test_file.exists()

    train_data = pd.read_json(train_file, lines=True)
    test_data = pd.read_json(test_file, lines=True)

    assert len(train_data) == 3
    assert len(test_data) == 1
