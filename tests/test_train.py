import sys
import tempfile
from pathlib import Path
from typing import Generator, Tuple

import pandas as pd
import pytest
from typer.testing import CliRunner

sys.path.append(str(Path(__file__).parent.parent))

from src.modeling.train import app, load_data, train_model  # noqa E402

runner = CliRunner()


@pytest.fixture
def sample_data() -> pd.DataFrame:
    """
    Fixture that provides sample data for testing.

    Returns:
        pd.DataFrame: Sample data containing ingredients and cuisine types.
    """
    data = [
        {"ingredients": "ingredient1 ingredient2", "cuisine": "cuisine1"},
        {"ingredients": "ingredient3 ingredient4", "cuisine": "cuisine1"},
        {"ingredients": "ingredient5 ingredient6", "cuisine": "cuisine2"},
        {"ingredients": "ingredient7 ingredient8", "cuisine": "cuisine2"},
    ]
    return pd.DataFrame(data)


@pytest.fixture
def temp_files(sample_data: pd.DataFrame) -> Generator[Tuple[Path, Path], None, None]:
    """
    Fixture that creates temporary files for training and testing.

    Args:
        sample_data (pd.DataFrame): The sample data to be written to files.

    Yields:
        Tuple[Path, Path]: Paths to the training file and testing file.
    """
    with tempfile.TemporaryDirectory() as temp_dir:
        train_file = Path(temp_dir) / "train.json"
        test_file = Path(temp_dir) / "test.json"

        sample_data.to_json(train_file, lines=True, orient="records")
        sample_data.to_json(test_file, lines=True, orient="records")

        yield train_file, test_file


def test_load_data(sample_data: pd.DataFrame, temp_files: Tuple[Path, Path]) -> None:
    """
    Test the load_data function to ensure it loads the data correctly.

    Args:
        sample_data (pd.DataFrame): The expected data.
        temp_files (Tuple[Path, Path]): The paths to the temporary files.
    """
    train_file, _ = temp_files
    df = load_data(train_file)
    pd.testing.assert_frame_equal(df, sample_data)


def test_train_model(temp_files: Tuple[Path, Path]) -> None:
    """
    Test the train_model function to ensure it trains the model correctly.

    Args:
        temp_files (Tuple[Path, Path]): The paths to the temporary files.
    """
    train_file, test_file = temp_files

    # Execute the training function
    train_model(train_file, test_file, n_splits=2)

    # No further assertions needed since we are not saving to a file


def test_main(temp_files: Tuple[Path, Path]) -> None:
    """
    Test the CLI application to ensure it runs without errors and trains the model.

    Args:
        temp_files (Tuple[Path, Path]): The paths to the temporary files.
    """
    train_file, test_file = temp_files

    result = runner.invoke(
        app,
        [
            "--train-file",
            str(train_file),
            "--test-file",
            str(test_file),
            "--n-splits",
            "2",
        ],
    )

    assert result.exit_code == 0
    assert "Model training complete" in result.stdout
